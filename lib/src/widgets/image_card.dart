import "package:flutter/material.dart";
import "../models/image.models.dart";

class ImageList extends StatelessWidget {
  final List<ImageModel> images;

  ImageList(this.images);

  @override
  Widget build(BuildContext context) {
    return images.isEmpty
        ? Text("No Images added")
        : ListView.builder(
            itemCount: this.images.length,
            itemBuilder: (context, int index) {
              return buildImage(images[index]);
            },
          );
  }

  Widget buildImage(ImageModel image) {
    return Container(
      margin: EdgeInsets.all(20.0),
      padding: EdgeInsets.all(20.0),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey,
        ),
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
              bottom: 20.0,
            ),
            child: Image.network(image.url),
          ),
          Text(image.title),
        ],
      ),
    );
  }
}
