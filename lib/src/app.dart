import "package:flutter/material.dart";
import "dart:convert";
import "dart:async";
import 'package:http/http.dart' show get;
import "models/image.models.dart";
import "widgets/image_card.dart";

// Two types of widget - Stateful and Stateless. 
class App extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return AppState();
  }

}


class AppState extends State<App> {
  int counter = 0;
  List<ImageModel> images = [];

  Future<void> onImageIconPress() async {
    counter++; 
    final response = await get(Uri.parse("https://jsonplaceholder.typicode.com/photos/$counter"));
    setState(() {
      images.add(ImageModel.fromJson(json.decode(response.body)));           
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("Let's see some Images"),
      ),
      body: ImageList(images),
      floatingActionButton: FloatingActionButton(
        onPressed: onImageIconPress,
        child: Icon(Icons.add_a_photo),
        tooltip: "Add Image",
        // foregroundColor: Color(),
      ),
    ));
  }

}
